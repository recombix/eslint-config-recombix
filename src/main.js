'use strict';

/**
 * @file MAIN
 * @description ESLint config used by Recombix and Voxis Engineering.
 */

module.exports = {

	extends: [
		"eslint:recommended",
		"plugin:node/recommended",
		"plugin:promise/recommended",
		"plugin:vue/recommended",
	],

	plugins: [
		"disable",
		"html",
		"json",
    "filenames",
		"node",
		"promise",
		"unicorn",
		"vue",
  ],

  parserOptions: {
		parser: "babel-eslint", // Allow ESLint to work with new/advanced ECMAScript features.
    ecmaVersion: 2019,
    sourceType: "script", // Default to script for Node.
    ecmaFeatures: {
      impliedStrict: false,
      jsx: true,
    },
  },

  env: {
		es6: true,
    browser: true,
		commonjs: true,
		node: true,
		serviceworker: true,
		worker: true,
		mocha: true,
		jquery: true,
  },

  rules: {

		/*
		 * ESLint Core Rules
		 */
		"array-bracket-spacing": [1, "always", {
			"singleValue": true,
			"objectsInArrays": false,
			"arraysInArrays": false,
		}],
    "arrow-body-style": [1, "as-needed"],
    "arrow-parens": 0,
    "brace-style": [2, "stroustrup", {
      "allowSingleLine": true
    }],
		"callback-return": [2, // Helps avoid silly developer mistakes.
			["callback", "cb", "finish", "done", "next", "nextItem"]
		],
    "camelcase": 1,
		"capitalized-comments": [1, "always", {
			"ignoreInlineComments": true,
		}],
    "comma-dangle": [2, { // Tidy git merges.
      "arrays": "always-multiline",
      "objects": "always-multiline",
      "imports": "always-multiline",
      "exports": "always-multiline",
      "functions": "always-multiline",
    }],
		"complexity": [2, {
			"max": 20,
		}],
    "consistent-return": 2, // Helps avoid silly developer mistakes.
    "curly": [2, "all"],
		"eol-last": [1, "always"], // Tidy whitespace.
    "func-names": [2, "as-needed", {
			"generators": "always",
		}],
    "global-require": 1,
    "guard-for-in": 2,
    "handle-callback-err": [2, "^err"], // Helps avoid silly developer mistakes.
    "id-length": [1, {
      "min": 2,
			"max": 40,
			"properties": "always",
    }],
    "indent": [2, "tab", {
      "SwitchCase": 1,
      "VariableDeclarator": {
        "var": 2,
        "let": 2,
        "const": 3,
      },
      "outerIIFEBody": 1,
      "MemberExpression": 1,
      "FunctionDeclaration": {
        "parameters": 1,
        "body": 1,
      },
      "FunctionExpression": {
        "parameters": 1,
        "body": 1,
      },
      "CallExpression": {
        "arguments": 1,
      },
    }],
		"max-classes-per-file": [2, 1],
    "max-depth": [2, 5],
    "max-len": [1, {
      "code": 120,
      "tabWidth": 2,
      "ignoreTrailingComments": true,
      "ignoreUrls": true,
    }],
		"max-lines": [2, 300],
		"max-lines-per-function": [2, {
			"max": 20,
			"skipBlankLines": true,
			"skipComments": true,
		}],
    "max-nested-callbacks": [2, 3],
    "max-params": [2, 5],
		"max-statements": [2, {
			"max": 15,
		}],
		"max-statements-per-line": [2, {
			"max": 1,
		}],
		"multiline-comment-style": [1, "starred-block"],
		"multiline-ternary": [2, "never"],
    "new-cap": [2, {
      "newIsCap": true,
      "capIsNew": true,
      "capIsNewExceptions": ["Router"],
    }],
		"no-async-promise-executor": 2,
		"no-await-in-loop": 0, // A perfectly sane language feature.
		"no-caller": 2, // Deprecated feature.
		"no-compare-neg-zero": 1, // Helps avoid silly developer mistakes.
		"no-cond-assign": [2, "except-parens"], // Helps avoid silly developer mistakes.
		"no-confusing-arrow": [2, { // Helps avoid silly developer mistakes.
			"allowParens": true,
		}],
		"no-console": 1, // Prefer to use a proper logger and clear out debug/test logs.
		"no-continue": 0, // A perfectly sane language feature.
		"no-debugger": 1, // Remember to clear these out!
		"no-else-return": 2,
		"no-lone-blocks": 2,
		"no-lonely-if": 2,
		"no-mixed-requires": [1, {
			"grouping": true,
			"allowCall": true,
		}],
		"no-new-require": 2,
		"no-param-reassign": [2, {
			"props": false,
		}],
		"no-path-concat": 2,
		"no-plusplus": 0,
		"no-process-exit": 2,
		"no-return-assign": 0,
		"no-return-await": 2, // Performance hit - "return await" wraps return value in an unnecessary additional promise.
		"no-script-url": 2,
		"no-self-compare": 2,
		"no-sequences": 2,
		"no-shadow": [2, {
			"allow": ["err"],
		}],
		"no-shadow-restricted-names": 2,
		"no-sync": 2,
		"no-tabs": 0,
		"no-template-curly-in-string": 1,
		"no-throw-literal": 2, // Throwing anything other than an error can result in unexpected behaviour.
		"no-undef-init": 2,
		"no-undefined": 2,
		"no-unused-vars": [2, {
			"vars": "all",
			"args": "after-used",
		}],
		"no-underscore-dangle": 0,
    "no-unexpected-multiline": 2,
    "no-unused-expressions": [2, {
      "allowTernary": true,
    }],
		"no-use-before-define": 2,
		"no-useless-computed-key": 2,
		"no-useless-concat": 2,
		"no-with": 2, // The "with" syntax is considered bad practice.
		"no-var": 2, // Use "const" or "let" as they are block scoped.
		"no-void": 0, // The "void (0)" statement is guaranteed to return "undefined".
		"prefer-template": 2,
		"quotes": [2, "backtick", {
			"avoidEscape": true,
		}],
    "object-curly-spacing": [1, "always"],
    "one-var": [2, "never"],
    "padded-blocks": 0,
		"padding-line-between-statements": [2,
			{ "blankLine": "always", "prev": "class", "next": "*" },
			{ "blankLine": "always", "prev": "directive", "next": "*" },
			{ "blankLine": "always", "prev": "iife", "next": "*" },
		],
    "prefer-arrow-callback": 1,
    "prefer-const": 2,
		"prefer-object-spread": 2,
		"prefer-promise-reject-errors": 2,
		"prefer-rest-params": 2,
    "prefer-spread": 2,
    "radix": [1, "as-needed"],
		"require-atomic-updates": 2,
		"require-await": 2,
		"require-jsdoc": [2, {
      "require": {
        "FunctionDeclaration": true,
        "MethodDefinition": true,
	      "ClassDeclaration": true,
        "ArrowFunctionExpression": false,
        "FunctionExpression": false,
      },
	  }],
    "semi": [2, "always"], // JS compiler must guess where to insert semicolons if not included.
		"semi-spacing": [1, { "before": false, "after": true }], // Tidy whitespace.
    "space-before-blocks": [1, {
      "functions": "always",
      "keywords": "always",
      "classes": "always",
    }],
    "space-before-function-paren": [1, {
      "anonymous": "always",
      "named": "always",
    }],
    "spaced-comment": 1,
    "strict": [2, "global"], // The "use strict" directive prevents silly mistakes.
		"symbol-description": 2,
		"template-curly-spacing": [2, "never"],
		"template-tag-spacing": [2, "always"],
		"valid-jsdoc": 2,
    "yoda": [2, "never"], // Helps avoid silly developer mistakes.

		/*
		 * eslint-plugin-filenames
		 */
		"filenames/match-regex": [2, /^[a-zA-Z][a-z0-9]*(?:[A-Z][a-z0-9]+)*(?:\.[a-z][a-z0-9]*(?:[A-Z][a-z0-9]+)*)*$/g], // file.js || my.file.js || myFile.js || MyFile.js || My.File.js
		"filenames/no-index": 2,
		"filenames/match-exported": 2,

		/*
		 * eslint-plugin-node
	 	 */
		"node/exports-style": [2, "module.exports"],
		"node/no-unpublished-require": ["error", {
			"allowModules": ["chai", "cucumber", "mocha", "nock"],
		}],
		"node/prefer-promises/fs": 2,
		"node/prefer-promises/dns": 2,
		"node/prefer-global/buffer": [2, "always"],
		"node/prefer-global/console": [2, "always"],
		"node/prefer-global/process": [2, "always"],
		"node/prefer-global/text-decoder": [2, "never"],
		"node/prefer-global/text-encoder": [2, "never"],
		"node/prefer-global/url-search-params": [2, "never"],
		"node/prefer-global/url": [2, "always"],

		/*
		 * eslint-plugin-promise
		 */
		"promise/catch-or-return": 2, // Helps avoid uncaught promise rejections.
		"promise/no-return-wrap": 2, // Performance hit - wraps return value in an unnecessary additional promise.
		"promise/param-names": 2, // Helps avoid silly developer mistakes.
		"promise/always-return": 0, // Disable a messy default rule in "eslint-plugin-promise".
		"promise/no-native": 0, // Rely on native promises instead of libraries.
		"promise/no-nesting": 2, // Helps avoid silly developer mistakes.
		"promise/no-promise-in-callback": 2, // Helps avoid silly developer mistakes.
		"promise/no-callback-in-promise": 2, // Helps avoid silly developer mistakes.
		"promise/avoid-new": 0, // Disable a useless default rule in "eslint-plugin-promise".
		"promise/no-new-statics": 2, // Helps avoid silly developer mistakes.
		"promise/no-return-in-finally": 2, // Avoid unexpected behaviour.
		"promise/valid-params": 2, // Helps avoid silly developer mistakes.
		"promise/prefer-await-to-then": 2, // Helps avoid silly developer mistakes.
		"promise/prefer-await-to-callbacks": 2, // Helps avoid silly developer mistakes.

		/*
		 * eslint-plugin-vue
		 */
		"vue/attribute-hyphenation": 0,
    "vue/html-indent": [2, "tab", {}],
    "vue/max-attributes-per-line": [2, {
      "singleline": 5,
    }],
    "vue/multiline-html-element-content-newline": 0,
		"vue/mustache-interpolation-spacing": [2, "never"],
		"vue/require-default-prop": 0,
		"vue/singleline-html-element-content-newline": 0,
    "vue/script-indent": [2, "tab", {
      "baseIndent": 1,
      "switchCase": 1,
		}],

		/*
		 * eslint-plugin-unicorn
		 */
		"unicorn/catch-error-name": [2, { // Tidyness and consistency.
			"name": "err",
		}],
		"unicorn/import-index": 2, // Tidyness and consistency.
		"unicorn/no-abusive-eslint-disable": 2, // Helps avoid silly developer mistakes.
		"unicorn/no-new-buffer": 2, // Don't use deprecated features.
		"unicorn/no-unsafe-regex": 2, // Performance and security.
		"unicorn/prefer-exponentiation-operator": 2, // Tidyness and consistency.
		"unicorn/prefer-includes": 2, // Tidyness and consistency.
		"unicorn/prefer-type-error": 2, // Tidyness and consistency.
		"unicorn/throw-new-error": 2, // Tidyness and consistency.
		"unicorn/regex-shorthand": 2, // Tidyness and consistency.

	},

	"overrides": [{
    "files": ["*.vue"],
    "rules": {
      "indent": 0, // Indenting is handled differently in Vue template files.
    },
  }],

};
