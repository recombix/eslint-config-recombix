'use strict';

/**
 * @file INDEX
 * @description ESLint seems to required an index file in the root of the module.
 */

module.exports = require(`./src/main`);
